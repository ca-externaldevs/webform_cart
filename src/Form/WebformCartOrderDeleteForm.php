<?php

namespace Drupal\webform_cart\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Webform cart order entity entities.
 *
 * @ingroup webform_cart
 */
class WebformCartOrderDeleteForm extends ContentEntityDeleteForm {


}
