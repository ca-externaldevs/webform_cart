<?php

namespace Drupal\webform_cart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform_cart\WebformCartSessionInterface;

/**
 * Provides a 'WebformCartCheckoutBlock' block.
 *
 * @Block(
 *  id = "webform_cart_checkout_block",
 *  admin_label = @Translation("Webform cart checkout block"),
 * )
 */
class WebformCartCheckoutBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\webprofiler\Entity\EntityManagerWrapper definition.
   *
   * @var \Drupal\webprofiler\Entity\EntityManagerWrapper
   */
  protected $entityTypeManager;
  /**
   * Drupal\webform_cart\WebformCartSessionInterface definition.
   *
   * @var \Drupal\webform_cart\WebformCartSessionInterface
   */
  protected $webformCartSession;

  /**
   * Constructs a new WebformCartCheckoutBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\webform_cart\WebformCartSessionInterface $webform_cart_session
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              WebformCartSessionInterface $webform_cart_session) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->webformCartSession = $webform_cart_session;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('webform_cart.session')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $order = '';
    $webform = '';
    $cartId = $this->webformCartSession->getCartIds();
    if ($cartId) {
      $orderEntity = $this->entityTypeManager
        ->getStorage('webform_cart_order')
        ->load($cartId[0]);

      $orderLineItems = $orderEntity->get('field_order_item')->getValue();
      foreach ($orderLineItems as $key => $value) {
        $orderLineIds[$key] = $value['target_id'];
      }

      $orderItemEntity = $this->entityTypeManager->getStorage('webform_cart_item')->loadMultiple($orderLineIds);
      $view_builder = $this->entityTypeManager->getViewBuilder('webform_cart_item');
      $order = $view_builder->viewMultiple($orderItemEntity);
      $targetWebform = $orderEntity->get('field_webform')->getValue();
      $webform_load = \Drupal\webform\Entity\Webform::load($targetWebform[0]['target_id']);
      $webform_build = \Drupal::entityTypeManager()->getViewBuilder('webform')->view($webform_load);
    }

    return [
      '#theme' => 'webform_cart_checkout',
      '#order' => $order,
      '#webform' => $webform_build,
      '#attached' => array(
        'library' => array(
          'webform_cart/webform_cart',
        ),
      ),
    ];
  }

}
