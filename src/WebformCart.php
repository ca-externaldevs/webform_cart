<?php

namespace Drupal\webform_cart;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class WebformCart.
 */
class WebformCart implements WebformCartInterface {

  private $cart;

  private $webformCartSession;

  private $entityTypeManager;

  private $entityFieldManager;


  /**
   * WebformCart constructor.
   *
   * @param \Drupal\webform_cart\WebformCartSessionInterface $webform_cart_session
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   */
  public function __construct(WebformCartSessionInterface $webform_cart_session,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityFieldManagerInterface $entity_field_manager) {
    $this->webformCartSession = $webform_cart_session;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * @return mixed
   */
  public function getCart() {
    return $this->cart;
  }

  /**
   * @param $orderItem
   *
   * @return int|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setCart($orderItem) {
    $cartId = $this->webformCartSession->getCartIds();

    // If CartId exists, load existing order.
    if ($cartId) {
      // Loop helps clean up any bad CartIds.
      foreach ($cartId as $value) {
        $orderEntity = $this->entityTypeManager->getStorage('webform_cart_order')
          ->load($value);
        // If cartId exists but order doesn't clear existing order id from session
        // To prevent multiple cartIds stored.
        if (!$orderEntity) {
          $this->webformCartSession->deleteCartId($value);
        }
      }
      $cartId = $this->webformCartSession->getCartIds();
    }

    // If no CartId or Order exists create new.
    if (empty($cartId) && !isset($orderEntity)) {
      $orderEntity = $this->entityTypeManager
        ->getStorage('webform_cart_order')
        ->create(['type' => $orderItem['order_type']]);
      $orderEntity->save();
      $this->webformCartSession->addCartId($orderEntity->id());
    }
    // Create Line for Order.
    $orderItemEntity = $this->addLineItem($orderEntity, $orderItem);
    // Add lineitem ID to Order Reference field.
    $orderEntity->field_order_item[] = $orderItemEntity->id();
    $orderEntity->save();
    $orderLineItems = $orderEntity->get('field_order_item')->getValue();
    $cartCount = count($orderLineItems);

    return $cartCount;
  }

  /**
   * @param $orderEntity
   * @param $orderItem
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function addLineItem($orderEntity, $orderItem) {

    $bundle_fields = $this->entityFieldManager
      ->getFieldDefinitions('webform_cart_order', $orderItem['order_type']);
    $field_name = 'field_order_item';
    $field_definition = $bundle_fields[$field_name];
    $target_bundle = $field_definition->getSettings()['handler_settings']['target_bundles'];
    foreach ($target_bundle as $value) {
      $orderItemEntity = $this->entityTypeManager
        ->getStorage('webform_cart_item')
        ->create(['type' => $value]);
      $name = $this->entityTypeManager->getStorage('node')->load($orderItem['node_id']);
      $orderItemEntity->set('name',$name->label());
      $orderItemEntity->set('order_id', $orderEntity->id());
      $orderItemEntity->set('original_product', $orderItem['node_id']);
      $orderItemEntity->set('quantity', $orderItem['quantity']);
      $orderItemEntity->set('data1', $orderItem['data1']);
      $orderItemEntity->set('data2', $orderItem['data2']);
      $orderItemEntity->save();
      break;
    }

    return $orderItemEntity;
  }

}
