<?php

namespace Drupal\webform_cart\Controller;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform_cart\WebformCartSessionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformCartCheckoutController.
 */
class WebformCartCheckoutController extends ControllerBase {

  protected $entityTypeManager;

  protected $webformCartSession;

  /**
   * WebformCartCheckoutController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\webform_cart\WebformCartSessionInterface $webform_cart_session
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, WebformCartSessionInterface $webform_cart_session) {
    $this->entityTypeManager = $entity_type_manager;
    $this->webformCartSession = $webform_cart_session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('webform_cart.session')
    );
  }

  /**
   * Reviewcart.
   *
   * @return string
   *   Return Hello string.
   */
  public function ReviewCart() {
    // TODO: Add Cart Review Modal.
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: ReviewCart')
    ];
  }


  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function confirmOrder() {

    $order = 'Order is empty';
    $webform_build = 'No form to complete';
    $cartId = $this->webformCartSession->getCartIds();
    if ($cartId) {
      $orderEntity = $this->entityTypeManager->getStorage('webform_cart_order')->load($cartId[0]);

      if ($orderEntity) {
        $orderLineIds = [];
        $orderLineItems = $orderEntity->get('field_order_item')->getValue();
        foreach ($orderLineItems as $key => $value) {
          $orderLineIds[$key] = $value['target_id'];
        }
        if ($orderLineIds) {
          $orderItemEntity = $this->entityTypeManager->getStorage('webform_cart_item')
            ->loadMultiple($orderLineIds);
          $view_builder = $this->entityTypeManager->getViewBuilder('webform_cart_item');
          $order = $view_builder->viewMultiple($orderItemEntity);
        }
        $targetWebform = $orderEntity->get('field_webform')->getValue();
        if ($targetWebform) {
          $webform_load = \Drupal\webform\Entity\Webform::load($targetWebform[0]['target_id']);
          $webform_build = \Drupal::entityTypeManager()
            ->getViewBuilder('webform')
            ->view($webform_load);
        }
      }
    }

    return [
      '#theme' => 'webform_cart_checkout',
      '#order' => $order,
      '#webform' => $webform_build,
      '#attached' => [
        'library' => [
          'webform_cart/webform_update_cart',
        ],
      ],
    ];
  }

  public function deleteItem($itemId) {

    $cartId = $this->webformCartSession->getCartIds();

    // If CartId exists, load existing order.
    if ($cartId) {
      // Loop helps clean up any bad CartIds.
      foreach ($cartId as $value) {
        $orderEntity = $this->entityTypeManager->getStorage('webform_cart_order')
          ->load($value);
        // If cartId exists but order doesn't clear existing order id from session
        // To prevent multiple cartIds stored.
        if (!$orderEntity) {
          $this->webformCartSession->deleteCartId($value);
        }
      }

      // Updated order entity lineitem reference.
      if ($orderEntity) {
        $orderLineIds = [];
        $orderLineItems = $orderEntity->get('field_order_item')->getValue();
        foreach ($orderLineItems as $key => $value) {
          if ($value['target_id'] != $itemId) {
            $orderLineIds[$key] = $value['target_id'];
          }
        }
        $orderEntity->field_order_item = $orderLineIds;
        $orderEntity->save();
      }

      // Delete line item.
      if ($itemId) {
        $orderItemEntity = $this->entityTypeManager->getStorage('webform_cart_item')->load($itemId);
        if ($orderItemEntity) {
          $orderItemEntity->delete();
        }
      }
    }

    $destination = 'webform_cart.webform_cart_checkout_controller_confirmOrder';
    return $this->redirect($destination);
  }

}
