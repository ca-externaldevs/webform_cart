<?php

namespace Drupal\webform_cart;

/**
 * Interface WebformCartInterface.
 */
interface WebformCartInterface {

  /**
   * @return mixed
   */
  public function getCart();

  /**
   * @param $order_item
   *
   * @return mixed
   */
  public function setCart($orderItem);


}
