<?php

namespace Drupal\webform_cart\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class UpdateCart.
 */
class UpdateCart implements CommandInterface {

  /**
   * Render custom ajax command.
   *
   * @return ajax
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'UpdateCart',
      'message' => 'My Awesome Message',
    ];
  }

}
